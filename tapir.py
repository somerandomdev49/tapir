import re, traceback, time

class List:
	def __init__(self, v: list[object]): self.v = v
	def __call__(self, n: int):
		try: return self.v[n]
		except IndexError: pass
		raise Exception(f"Index {n} is out of range for list " + repr(self))
			
	def __repr__(self): return repr(tuple(self.v))

class Func:
	def __init__(self, u: callable, r: str): self.u = u; self.r = r
	def __call__(self, *args: object) -> object: return self.u(*args)
	def __repr__(self): return self.r

class MemoFunc(Func):
	def __init__(self, u: callable, r: str):
		super().__init__(u, r)
		self.m = {}

	def __call__(self, *args: object) -> object:
		if args in self.m: return self.m[args]
		v = self.m[args] = self.u(*args)
		return v\

class Nil:
	def __repr__(self): return "nil"

STDLIB = {
	"+": lambda a, b: a + b,
	"-": lambda a, b: a - b,
	"*": lambda a, b: a * b,
	"/": lambda a, b: a / b,
	">": lambda a, b: a > b,
	"<": lambda a, b: a < b,
	"<=": lambda a, b: a <= b,
	">=": lambda a, b: a >= b,
	"==": lambda a, b: a == b,
	"!=": lambda a, b: a != b,
	"~": lambda a: not a,
	"bool": lambda a: bool(a),
	"&": lambda a, b: a and b,
	"|": lambda a, b: a or b,
	"nil": Nil(),
	"list": lambda *v: List(v),
	"slice-start": lambda v, a: List(v.v[a:]),
	"slice-start-end": lambda v, a, b: List(v.v[a:b]),
	"slice-end": lambda v, a: List(v.v[:a]),
	"error": lambda r: print("ERROR:", r),
}

class CommandResult:
	def __init__(self, msg: str):
		self.msg = msg

class CommandError:
	pass

def run(
	s: str,
	ctx: dict[str, object] = STDLIB.copy(),
	initctx: dict[str, object] = None,
) -> object:
	if initctx is None: initctx = ctx.copy()

	AKWDS = list("[]{}()\\")
	s = re.sub(r"--.*\n", '', s)
	s = re.sub(r"//.*\n", '', s)
	slit_idx = 0
	def f(m: re.Match):
		nonlocal slit_idx; slit_idx += 1
		ctx[f"\"{slit_idx}"] = m.group(1)
		return f"\"{slit_idx}"
	s = re.sub(r"\"([^\"]*)\"", f, s)
	for kwd in AKWDS: s = s.replace(kwd, f" {kwd} ")
	s = re.sub(r"\s+", ' ', s)
	toks = list(filter(lambda x: len(x), s.split(' ')))
	toks.append('\0')

	had_errs = False

	class Parser:
		def __init__(self, toks: list[str], i: int):
			self.toks = toks
			self.i = i
		
		def c(self, n: int = 0) -> str:
			return self.toks[self.i + n] if self.i + n < len(self.toks) else "\0"
		def n(self) -> str: self.i += 1; return self.c(-1)

	def error(msg: str):
		print("\033[0;31merror:\033[0;0m", msg)
		nonlocal had_errs; had_errs = True
		# exit(1)

	def parse_atom(p: Parser):
		t = p.n()
		if t == '(':
			e = parse_expr(p)
			if p.n() != ')': error("expected ')'")
			return e
		elif t == ')': error("unexpected ')'")
		elif t == '$': return parse_expr(p)
		elif t == '\\':
			a = p.n()
			if p.c() != ':':
				error("expected ':'")
			else: p.n()
			v = parse_expr(p)
			return ("l", a, v)
		elif t == '\0': error("unexpected EOI")
		else: return ("b", t)

	KWS = [')', '\0', ':', '?', '!', '.']

	def parse_expr(p: Parser):
		a = [parse_atom(p)]
		while p.c() not in KWS:
			a.append(parse_atom(p))
		
		if len(a) == 1: v = a[0]
		else: v = ("a", *a)

		if len(a) == 1 and p.c() == '!':
			p.n()
			v = ("a", v)
			
		if p.c() == '?':
			p.n()
			t = parse_expr(p)
			if p.n() != ':': error("expected ':'")
			e = parse_expr(p)
			v = ("q", v, t, e)
			
		return v
	
	def parse_func(p: Parser):
		n = p.n()
		a = []

		if p.c() == ':':
			p.n()
			return ("c", n, parse_expr(p))

		while p.c() != '=' and p.c() not in KWS:
			a.append(p.n())
		
		if p.c() != '=':
			error("expected '='")
		else: p.n()

		e = parse_expr(p)
		return ('f', n, a, e)


	def printt(t, l: int = 0):
		i = 1
		print("  " * l + t[0], end=' ')
		while i < len(t) and isinstance(t[i], str):
			print(t[1], end=' ')
			i += 1

		print()
		for e in t[i:]: printt(e, l + 1)

	def norm(t):
		if t[0] == "a" and len(t) == 2:
			return norm(t[1])
		if t[0] == "b":
			return t
		if t[0] == "q":
			return ("q", norm(t[1]), norm(t[2]), norm(t[3]))
		if t[0] == "f":
			return ("f", t[1], t[2], norm(t[3]))
		return t

	def print_stt(stt: list[str]):
		print("Stacktrace:")
		for e in stt:
			print("\t", e)

	def _run(t, c, stt: list[str]):
		if t[0] == "a":
			v = _run(t[1], c, stt)
			if not callable(v):
				error(f"Value {v} is not callable!")
				print_stt(stt)
				return None
			return v(*map(lambda x: _run(x, c, stt), t[2:]))
		elif t[0] == "b":
			try: return int(t[1])
			except ValueError: pass
			try: return float(t[1])
			except ValueError: pass
			if t[1] not in c:
				error(f"unknown binding: '{t[1]}'")
				return None
			return c[t[1]]
		elif t[0] == "c":
			if t[1] in c:
				error(f"duplicate binding: '{t[1]}'")
			c[t[1]] = _run(t[2], c, stt)
			return None
		elif t[0] == "q":
			q = _run(t[1], c, stt)
			return _run(t[2], c, stt) if q else _run(t[3], c, stt)
		elif t[0] == "f":
			def _f(*args):
				stt.append(t[1])
				c2 = { **c, **({ k:v for k, v in zip(t[2], args) }) }
				r = _run(t[3], c2, stt)
				stt.pop()
				return r
			c[t[1]] = Func(_f, f"function '{t[1]}'")
		elif t[0] == "l":
			def _f(*args):
				stt.append("<lambda>")
				r = _run(t[2], { **c, t[1]: args[0] }, stt)
				stt.pop()
				return r
			return Func(_f, "<lambda>")
		elif t[0] == ":memoize":
			t2 = t[1]
			def _f(*args):
				stt.append(t2[1])
				c2 = { **c, **({ k:v for k, v in zip(t2[2], args) }) }
				r = _run(t2[3], c2, stt)
				stt.pop()
				return r
			c[t2[1]] = MemoFunc(_f, f"function:memoized '{t2[1]}'")

	def docode(ps: Parser):
		if ps.c() == '=':
			ps.n()
			ast = parse_expr(ps)
		elif ps.c().startswith(':'):
			if ps.c() == ":load-file":
				ps.n()
				with open(ps.n()) as f:
					return run(f.read(), ctx)
			elif ps.c() == ":reload":
				ctx.clear()
				ctx.update(initctx)
				return CommandResult("Reloaded!")
			elif ps.c() == ":reload-file":
				ps.n()
				with open(ps.n()) as f:
					ctx.clear()
					ctx.update(initctx)
					return run(f.read(), ctx)
			elif ps.c() == ":memoize":
				ps.n()
				ast = (":memoize", parse_func(ps))
			elif ps.c() == ":time":
				ps.n()
				tbegin = time.time()
				v = docode(ps)
				tend = time.time()
				print("Took", tend - tbegin)
				return v
			else:
				error(f"unknown command: '{ps.c()}'")
				return CommandError()
		else:
			ast = parse_func(ps)
		
		if ast is None:
			error("internal error occured (ast is None)")
			return None
		
		if had_errs:
			return Exception()

		ast = norm(ast)
		# printt(ast)
		stt = []
		try:
			return _run(ast, ctx, stt)
		except Exception as e:
			traceback.print_exc()
			error(f"An error occured ({type(e).__name__})!")
			print_stt(stt)
		
	ps = Parser(toks, 0)
	v = docode(ps)
	while ps.c() == '.':
		ps.n()
		if ps.c() == "\0": break
		v = docode(ps)
	
	return v

	# n = 5
	# print(f"fib {n} =", c["fib"](n))

from typing import Iterator
import prompt_toolkit as pt
import prompt_toolkit.lexers as pt_lex
import prompt_toolkit.styles.style as pt_stl
import prompt_toolkit.styles.pygments as pt_spgs
import prompt_toolkit.formatted_text as pt_fmt
import pygments as pgs
from pygments.styles import get_style_by_name as pgs_get_stn
import pygments.lexer as pgs_lex
from pygments.lexer import RegexLexer as PgsRegexLexer
import pygments.token as pgst
from pygments.styles.bw import BlackWhiteStyle as pgsstyle
import tapir

class Lexer(PgsRegexLexer):
	name = "Tapir"
	aliases = ["tapir"]
	filenames = ["*.tapir"]

	tokens = {
		"root": [
			(r"--.*$", pgst.Comment),
			(r"//.*$", pgst.Comment),
			(r"\"[^\"]*\"", pgst.String),
			(r":[^ ]+", pgst.Keyword),
			(r"(\s|\)|\(|\]|\[|\}|\{|^)((0[xob])?([0-9]+)(\.[0-9]+)?)", pgst.Number),
			(r"[\(\)\[\]\{\}\.\!\?\:\$]", pgst.Punctuation),
		]
	}
		


@(lambda f: f())
def main():
	# st = pt_spgs.style_from_pygments_cls(pgsstyle)
	st = pt_stl.Style([
		("pygments.keyword", "bold underline ansiwhite"),
		("pygments.comment", "italic ansigray"),
		("pygments.punctuation", "ansibrightblack"),
		("pygments.string", "italic ansibrightblack")
	])
	session = pt.PromptSession(
		lexer = pt_lex.PygmentsLexer(Lexer),
		style = st
	)

	ctx = tapir.STDLIB.copy()
	initctx = ctx.copy()
	try:
		while True:
			s = session.prompt(pt_fmt.HTML(f"<ansiyellow>tapir</ansiyellow> → ")).strip()
			if len(s) == 0: continue
			r = tapir.run(s, ctx, initctx)
			if isinstance(r, Exception): continue
			if isinstance(r, tapir.CommandResult): print(r.msg); continue
			if isinstance(r, tapir.CommandError): continue
			print(r)
	except KeyboardInterrupt:
		exit(1)